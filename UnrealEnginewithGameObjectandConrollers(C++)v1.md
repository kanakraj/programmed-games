# Unreal Engine C++ Code: Custom Actor with Input Handling

This code creates a custom actor in Unreal Engine using C++. The actor includes components such as a static mesh, a spring arm, and a camera. It also handles user input for movement and rotation.

## Features

1. **Components:**
   - `UStaticMeshComponent`: Represents the visible mesh of the actor.
   - `USpringArmComponent`: Provides a spring arm for smooth camera movement.
   - `UCameraComponent`: Represents the actor's camera.

2. **Input Handling:**
   - The actor responds to user input for movement and rotation.
   - Input is handled using the `InputComponent` and Axis Binding in Unreal Engine.

3. **Movement:**
   - The actor moves forward, backward, left, and right based on user input.
   - Rotation around the Yaw axis is controlled by user input.

## How to Use

1. **Compilation:**
   - Compile the code using a C++ compiler (e.g., Visual Studio for Windows or Xcode for macOS).
   - Ensure that the necessary Unreal Engine headers and libraries are configured.

2. **Gameplay:**
   - Add an instance of the `AMyCustomActor` class to your level in the Unreal Editor.
   - Run the game to see the actor in action.
   - Use keyboard input to move the actor forward, backward, left, and right.
   - Rotate the actor around the Yaw axis using the respective input.

## Code Structure

### MyCustomActor.h

- Defines the `AMyCustomActor` class, a subclass of `AActor`.
- Includes declarations for the custom components, movement speed, rotation speed, and input handling functions.

### MyCustomActor.cpp

- Implements the functions declared in `MyCustomActor.h`.
- Sets up components (mesh, spring arm, camera) and handles user input for movement and rotation.
- Contains the main logic for updating the actor's position and rotation based on input.

## Notes

- This example is a starting point for creating a custom actor with user input in Unreal Engine using C++.
- Customize the actor's components, appearance, and behavior based on your project requirements.
- Ensure that you have Unreal Engine installed and configured before attempting to compile and run the code.

