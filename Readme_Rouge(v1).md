# Text-Based Rogue-like Game in C++

This is a text-based rogue game implemented in C++. The game involves a player navigating through a dungeon, encountering monsters, and battling them.

## Features

1. **Player Stats:**
   - Health: 100
   - Attack: 10
   - Level: 1

2. **Monster Stats:**
   - Health: 20
   - Attack: 5

3. **Dungeon Grid:**
   - The dungeon is represented as a 5x5 grid.
   - The player 'P' and monster 'M' are placed randomly in the dungeon.
   - The player can move through the dungeon.

4. **Combat:**
   - The player engages in turn-based combat with the monster.
   - The monster attacks the player, reducing health.

## How to Play

1. **Compilation:**
   - Compile the code using a C++ compiler (e.g., g++).
   - Ensure that the required libraries (iostream, cstdlib, ctime) are available.

2. **Gameplay:**
   - Run the compiled executable.
   - Enter moves to navigate through the dungeon:
     - 1: Up
     - 2: Down
     - 3: Left
     - 4: Right

3. **Objective:**
   - Survive the monster attacks and explore the dungeon.
   - The game ends when the player's health reaches 0.

## Sample Gameplay

```plaintext
. . . . .
. . . . .
. . . . .
. . . . .
. . . . .

Level: 1 | Health: 100 | Attack: 10
Enter move (1: Up, 2: Down, 3: Left, 4: Right): 4

. . . . .
. . . . .
. . . . .
. . P . .
. . . . .

Level: 1 | Health: 95 | Attack: 10
Enter move (1: Up, 2: Down, 3: Left, 4: Right): 2

M . . . .
. . . . .
. . P . .
. . . . .
. . . . .

Level: 1 | Health: 90 | Attack: 10
...
Game Over. You lost.
