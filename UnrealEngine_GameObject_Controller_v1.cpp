// MyCustomActor.h

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "MyCustomActor.generated.h"

UCLASS()
class YOURPROJECT_API AMyCustomActor : public AActor
{
	GENERATED_BODY()

public:
	AMyCustomActor();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float MoveSpeed;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float RotateSpeed;

	FVector CurrentVelocity;

	void MoveForward(float Value);
	void MoveRight(float Value);
	void RotateYaw(float Value);
};
// MyCustomActor.cpp

#include "MyCustomActor.h"
#include "GameFramework/PlayerController.h"

AMyCustomActor::AMyCustomActor()
{
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->SetupAttachment(MeshComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	MoveSpeed = 500.0f;
	RotateSpeed = 100.0f;
}

void AMyCustomActor::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		EnableInput(GetWorld()->GetFirstPlayerController());
		InputComponent->BindAxis("MoveForward", this, &AMyCustomActor::MoveForward);
		InputComponent->BindAxis("MoveRight", this, &AMyCustomActor::MoveRight);
		InputComponent->BindAxis("RotateYaw", this, &AMyCustomActor::RotateYaw);
	}
}

void AMyCustomActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Move the actor
	FVector NewLocation = GetActorLocation() + (CurrentVelocity * MoveSpeed * DeltaTime);
	SetActorLocation(NewLocation);

	// Rotate the actor
	FRotator NewRotation = GetActorRotation();
	NewRotation.Yaw += CurrentVelocity.X * RotateSpeed * DeltaTime;
	SetActorRotation(NewRotation);
}

void AMyCustomActor::MoveForward(float Value)
{
	CurrentVelocity.X = FMath::Clamp(Value, -1.0f, 1.0f);
}

void AMyCustomActor::MoveRight(float Value)
{
	CurrentVelocity.Y = FMath::Clamp(Value, -1.0f, 1.0f);
}

void AMyCustomActor::RotateYaw(float Value)
{
	CurrentVelocity.X = FMath::Clamp(Value, -1.0f, 1.0f);
}
