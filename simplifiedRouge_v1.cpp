#include <iostream>
#include <cstdlib>
#include <ctime>

class Player {
public:
    int health;
    int attack;
    int level;

    Player() : health(100), attack(10), level(1) {}

    void displayStats() const {
        std::cout << "Level: " << level << " | Health: " << health << " | Attack: " << attack << std::endl;
    }
};

class Monster {
public:
    int health;
    int attack;

    Monster() : health(20), attack(5) {}

    void displayStats() const {
        std::cout << "Monster | Health: " << health << " | Attack: " << attack << std::endl;
    }
};

class Dungeon {
public:
    char grid[5][5];

    Dungeon() {
        // Initialize the dungeon grid
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                grid[i][j] = '.';
            }
        }
    }

    void display() const {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                std::cout << grid[i][j] << ' ';
            }
            std::cout << std::endl;
        }
    }

    void placePlayer(int x, int y) {
        grid[x][y] = 'P';
    }

    void placeMonster(int x, int y) {
        grid[x][y] = 'M';
    }

    void clearCell(int x, int y) {
        grid[x][y] = '.';
    }
};

int main() {
    srand(static_cast<unsigned>(time(nullptr)));

    Player player;
    Dungeon dungeon;

    // Place the player and a monster in the dungeon
    dungeon.placePlayer(0, 0);
    dungeon.placeMonster(rand() % 5, rand() % 5);

    while (player.health > 0) {
        dungeon.display();
        player.displayStats();

        int move;
        std::cout << "Enter move (1: Up, 2: Down, 3: Left, 4: Right): ";
        std::cin >> move;

        // Implement player movement logic here

        // Simulate monster attack
        player.health -= dungeon.grid[0][1] == 'M' ? 5 : 0;

        // Clear the dungeon grid
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                dungeon.clearCell(i, j);
            }
        }

        // Place the player and a new monster in the dungeon
        dungeon.placePlayer(rand() % 5, rand() % 5);
        dungeon.placeMonster(rand() % 5, rand() % 5);
    }

    std::cout << "Game Over. You lost." << std::endl;

    return 0;
}
